defmodule Elixip do
  def delay(extra \\ 1) do
    rand = :rand.uniform(extra)
    time_to_wait = 1000 + rand

    Process.sleep(time_to_wait)

    {:ok, time_to_wait}
  end
end
