defmodule Elixip.Endpoint do
  @callback processor() :: atom()

  defmacro __using__(_opts) do
    quote do
      @behaviour Elixip.Endpoint

      import unquote(__MODULE__)

      use GenServer

      # Client

      def start_link(config) do
        name = {:via, Registry, {Elixip.Channels, Keyword.get(config, :name)}}

        {:ok, _} = GenServer.start_link(__MODULE__, config, name: name)
      end

      @impl true
      def init(config) do
        supporting_processes = apply(processor(), :supporting_processes, [])

        Enum.each(supporting_processes, fn child_spec ->
          Elixip.Processor.Supervisor.spawn_child(child_spec)
        end)

        {:ok, config}
      end

      @impl true
      def handle_call(message, _from,  config) do
        {:reply, message, config}
      end

      @impl true
      def handle_cast(message, config) do
        spawn_message_processor(message, config)

        {:noreply, config}
      end

      def spawn_message_processor(message, config) do
        child_spec = {processor(), {message, config}}

        Elixip.Processor.Supervisor.spawn_child(child_spec)
      end

      def processor do
      end

      defoverridable [processor: 0]
    end
  end
end
