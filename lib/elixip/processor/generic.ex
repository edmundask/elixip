defmodule Elixip.Processor.Generic do
  @callback process(message :: Elixip.Message.t) :: Elixip.Message.t

  defmacro __using__(_opts) do
    quote do
      @behaviour Elixip.Processor.Generic

      import unquote(__MODULE__)

      @spec process(Elixip.Message.t) :: Elixip.Message.t
      def process(message) do
        message
      end

      def run({message, config}) do
        processed_message = process(message)

        Elixip.Channel.transport(Keyword.get(config, :to), processed_message)

        {:ok, processed_message}
      end

      defoverridable [process: 1]
    end
  end
end
