defmodule Elixip.Processor.Router do
  @callback route(message :: Elixip.Message.t, routes :: struct()) :: any()

  defmacro __using__(_opts) do
    quote do
      @behaviour Elixip.Processor.Router

      import unquote(__MODULE__)

      @spec route(Elixip.Message.t, struct()) :: any()
      def route(message, routes) do
      end

      def run({message, config}) do
        routed_channel = route(message, Keyword.get(config, :routes))

        Elixip.Channel.transport(routed_channel, message)

        {:ok, message}
      end

      defoverridable [route: 2]
    end
  end
end
