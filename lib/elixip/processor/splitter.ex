defmodule Elixip.Processor.Splitter do
  @callback split(message :: Elixip.Message.t) :: list(any())

  defmacro __using__(_opts) do
    quote do
      @behaviour Elixip.Processor.Splitter

      import unquote(__MODULE__)

      @spec split(Elixip.Message.t) :: list(any())
      def split(message) do
        [message]
      end

      def run({message, config}) do
        messages = split(message)
        messages = produce_list(messages)

        Enum.each(messages, fn message ->
          Elixip.Channel.transport(Keyword.get(config, :to), message)
        end)

        {:ok, messages}
      end

      defoverridable [split: 1]
    end
  end

  @spec produce(any()) :: Elixip.Message.t()
  def produce(element) do
    case element do
      %Elixip.Message{} = message ->
        message
      body ->
        Elixip.Message.create(body)
    end
  end

  @spec create_enhancer(list(any())) :: (Elixip.Message.t  -> Elixip.Message.t)
  def create_enhancer(elements) do
    correlation_id = UUID.uuid4()
    sequence_size = length(elements)

    fn message ->
      message = Elixip.Message.put_header(message, "sender", self())
      message = Elixip.Message.put_header(message, "items", sequence_size)
      message = Elixip.Message.set_correlation_id(message, correlation_id)
      message = Elixip.Message.set_sequence_size(message, sequence_size)

      message
    end
  end

  @spec produce_list(list(any())) :: list(Elixip.Message.t())
  def produce_list(elements) do
    enhance = create_enhancer(elements)

    elements
    |> Enum.map(&produce/1)
    |> Enum.map(enhance)
  end
end
