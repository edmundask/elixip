defmodule Elixip.Processor.Aggregator.Storage do
  use Agent

  def start_link(opts) do
    {initial_value, opts} = Keyword.pop(opts, :initial_value, %{})

    Agent.start_link(fn -> initial_value end, opts)
  end

  def get_correlating_messages(storage, correlation_id) do
    Agent.get(storage, fn aggregates ->
      get_in(aggregates, [correlation_id])
    end) || []
  end

  def put_correlating_mesage(storage, message, correlation_id) do
    messages = get_correlating_messages(storage, correlation_id)

    Agent.update(storage, fn aggregates ->
      put_in(aggregates, [correlation_id], [message | messages])
    end)
  end
end
