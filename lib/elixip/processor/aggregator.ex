defmodule Elixip.Processor.Aggregator do
  @callback aggregate(messages :: [Elixip.Message.t]) :: Elixip.Message.t
  @callback correlate(message :: Elixip.Message.t) :: atom
  @callback complete?(messages :: [Elixip.Message.t]) :: boolean

  defmacro __using__(opts) do
    {storage, _opts} = Keyword.pop(opts, :storage, Elixip.Processor.Aggregator.Storage)

    quote do
      @behaviour Elixip.Processor.Aggregator

      import unquote(__MODULE__)

      @storage unquote(storage)

      def aggregate(messages) do
        List.last(messages)
      end

      def correlate(message) do
        Elixip.Message.get_correlation_id(message)
      end

      def complete?(messages) when length(messages) == 0 do
        false
      end

      def complete?(messages) do
        message = List.first(messages)
        sequence_size = Elixip.Message.get_sequence_size(message) || 0

        length(messages) == sequence_size
      end

      def run({message, config}) do
        correlation_id = correlate(message)

        @storage.put_correlating_mesage(@storage, message, correlation_id)
        messages = @storage.get_correlating_messages(@storage, correlation_id)

        if complete?(messages) do
          message = aggregate(messages)
          message = Elixip.Message.put_header(message, "sender", self())

          Elixip.Channel.transport(Keyword.get(config, :to), message)
        end

        {:ok, messages}
      end

      def supporting_processes do
        [
          {unquote(storage), name: unquote(storage)}
        ]
      end

      defoverridable [aggregate: 1, correlate: 1, complete?: 1]
    end
  end

  defmacro aggregation(function) do
    quote do
      def aggregate(messages) do
        unquote(function).(messages)
      end
    end
  end

  defmacro correlation(function) do
    quote do
      def correlate(message) do
        unquote(function).(message)
      end
    end
  end

  defmacro completeness(function) do
    quote do
      def complete?(messages) do
        unquote(function).(messages)
      end
    end
  end
end
