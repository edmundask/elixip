defmodule Elixip.Message do
  @correlation_header_name "CORRELATION_ID"
  @sequence_size_header_name "SEQUENCE_SIZE"
  @message_id_header_name "MESSAGE_ID"

  defstruct headers: %{},
            body: nil

  @type t(body) :: %__MODULE__{body: body}

  @spec create(any(), map()) :: __MODULE__.t()
  def create(body \\ nil, headers \\ %{}) do
    message = %__MODULE__{body: body, headers: headers}

    set_mesage_id(message)
  end

  @spec get_body(Elixip.Message.t()) :: any()
  def get_body(message) do
    message.body
  end

  @spec put_body(Elixip.Message.t(), any()) :: Elixip.Message.t()
  def put_body(%__MODULE__{} = message, body) do
    %{message | body: body}
  end

  def get_header(%__MODULE__{headers: headers}, key) when is_binary(key) do
    get_in(headers, [key])
  end

  @spec put_header(__MODULE__.t(), String.t(), any) :: __MODULE__.t()
  def put_header(%__MODULE__{headers: headers} = message, key, value) when is_binary(key) do
    %{message | headers: put_in(headers, [key], value)}
  end

  @spec set_mesage_id(__MODULE__.t, String.t) :: __MODULE__.t
  def set_mesage_id(message, message_id \\ UUID.uuid4()) do
    put_header(message, @message_id_header_name, message_id)
  end

  @spec get_message_id(__MODULE__.t) :: String.t | nil
  def get_message_id(message) do
    get_header(message, @message_id_header_name)
  end

  @spec set_correlation_id(__MODULE__.t, String.t) :: __MODULE__.t
  def set_correlation_id(message, correlation_id \\ UUID.uuid4()) do
    put_header(message, @correlation_header_name, correlation_id)
  end

  @spec get_correlation_id(__MODULE__.t) :: String.t | nil
  def get_correlation_id(message) do
    get_header(message, @correlation_header_name)
  end

  @spec set_sequence_size(__MODULE__.t, integer) :: __MODULE__.t
  def set_sequence_size(message, sequence_size \\ 0) do
    put_header(message, @sequence_size_header_name, sequence_size)
  end

  @spec get_sequence_size(__MODULE__.t) :: String.t | nil
  def get_sequence_size(message) do
    get_header(message, @sequence_size_header_name)
  end
end
