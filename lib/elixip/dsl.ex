defmodule Elixip.DSL do
  defmacro __using__(_opts) do
    quote do
      import unquote(__MODULE__)

      Module.register_attribute(__MODULE__, :endpoints, accumulate: true)
    end
  end

  defmacro message_with_body(body) do
    quote do
      %Elixip.Message{body: unquote(body)}
    end
  end

  defmacro message_with_headers(headers) do
    quote do
      %Elixip.Message{headers: unquote(headers)}
    end
  end

  defmacro from(channel, _opts \\ [], do: block) do
    module_name = channel
    |> String.split(":")
    |> Enum.map(fn part -> String.capitalize(part) end)
    |> Enum.join
    |> String.to_atom

    quote do
      namespace = Module.concat(__MODULE__, unquote(module_name))
      Module.put_attribute(__MODULE__, :options, namespace: namespace, channel: unquote(channel))

      unquote(block)

      endpoint_module_body =
        quote do
          use Elixip.Endpoint

          def processor do
            Module.concat(unquote(namespace), Processor)
          end
        end

      Module.create(Module.concat(namespace, Endpoint), endpoint_module_body, Macro.Env.location(__ENV__))
    end
  end

  defmacro splitter(func) do
    quote do
      options = Module.get_attribute(__MODULE__, :options)
      namespace = Keyword.get(options, :namespace)
      module_name = Module.concat(namespace, Processor)

      defmodule module_name do
        use Elixip.Processor
        use Elixip.Processor.Splitter

        def split(message) do
          unquote(func).(message)
        end
      end
    end
  end

  defmacro router(func) do
    expression = quote do
      options = Module.get_attribute(__MODULE__, :options)
      namespace = Keyword.get(options, :namespace)
      module_name = Module.concat(namespace, Processor)

      defmodule module_name do
        use Elixip.Processor
        use Elixip.Processor.Router

        def route(message, routes) do
          unquote(func).(message)
        end
      end
    end

    define_processor(expression)
  end

  defmacro aggregator(func) do
    expression = quote do
      options = Module.get_attribute(__MODULE__, :options)
      namespace = Keyword.get(options, :namespace)
      module_name = Module.concat(namespace, Processor)

      defmodule module_name do
        use Elixip.Processor
        use Elixip.Processor.Aggregator

        @quoted unquote(func).()

        unquote(@quoted)
      end
    end
  end

  defmacro processor(func) do
    expression = quote do
      options = Module.get_attribute(__MODULE__, :options)
      namespace = Keyword.get(options, :namespace)
      module_name = Module.concat(namespace, Processor)

      defmodule module_name do
        use Elixip.Processor
        use Elixip.Processor.Generic

        def process(message) do
          unquote(func).(message)
        end
      end
    end
  end

  defmacro to(destination, opts \\ [])

  defmacro to(destination, opts) when is_binary(destination) do
    expression = Keyword.get(opts, :using)

    define_processor(expression, destination)
  end

  # defmacro to(destination, opts) when is_atom(destination) do
  # end

  # defmacro to(destination, opts) when is_function(destination) do
  # end

  defmacro body(accessor) do
    quote do
      fn message ->
        Map.get(Elixip.Message.get_body(message), unquote(accessor))
      end
    end
  end

  defp define_processor(expression, destination \\ :none) do
    quote do
      options = Module.get_attribute(__MODULE__, :options)
      namespace = Keyword.get(options, :namespace)
      channel = Keyword.get(options, :channel)

      endpoint_module_name = Module.concat(namespace, Endpoint)

      Module.put_attribute(__MODULE__, :endpoints, {endpoint_module_name, [name: channel, to: unquote(destination)]})

      unquote(expression)
    end
  end
end
