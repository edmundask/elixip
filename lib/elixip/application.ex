defmodule Order do
  defstruct items: [], id: nil
end

defmodule OrderItem do
  defstruct is_iced: false, order_id: nil
end

defmodule OrderProducer do
  def start do
    do_produce()
  end

  def do_produce do
    message = generate_order_message()

    Elixip.Channel.transport("orders", message)
    Process.sleep(1000)

    do_produce()
  end

  def generate_order_message do
    id = :rand.uniform(1000)
    order = %Order{id: id, items: [%OrderItem{is_iced: true, order_id: id}, %OrderItem{order_id: id}]}

    Elixip.Message.create(order)
  end
end
