defmodule Elixip.Integration do
  defmacro __using__(opts) do
    {name, _opts} = Keyword.pop(opts, :name, Integration)

    quote do
      import unquote(__MODULE__)

      use Application

      def start(_type, _args) do
        children = [
          {Registry, keys: :unique, name: Elixip.Channels},
          {Elixip.Processor.Supervisor, []},
          {Elixip.Endpoint.Supervisor, endpoints()}
        ]

        supervisor_opts = [strategy: :one_for_one, name: unquote(name)]

        Supervisor.start_link(children, supervisor_opts)
      end

      def endpoints do
        []
      end

      defoverridable(endpoints: 0)
    end
  end
end
