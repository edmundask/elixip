defmodule Elixip.Processor do
  @callback run({message :: Elixip.Message.t, config :: struct()}) :: any()
  @callback supporting_processes() :: list(any())

  defmacro __using__(_opts) do
    quote do
      @behaviour Elixip.Processor

      import unquote(__MODULE__)

      use GenServer

      def start_link(state) do
        GenServer.start_link(__MODULE__, state)
      end

      def init(init_arg) do
        schedule_run()

        {:ok, init_arg}
      end

      def handle_info(:run, {message, config} = state) do
        run({ message, config })

        {:stop, :normal, state}
      end

      def child_spec(opts) do
        %{
          id: __MODULE__,
          start: {__MODULE__, :start_link, [opts]},
          restart: :transient,
        }
      end

      def supporting_processes do
        []
      end

      defp schedule_run do
        Process.send_after(self(), :run, 0)
      end

      defoverridable [supporting_processes: 0]
    end
  end
end
