defmodule Elixip.Channel do
  def channel_name_spec(name) do
    {:via, Registry, {Elixip.Channels, name}}
  end

  def transport(channel, message) do
    GenServer.cast(channel_name_spec(channel), message)
  end

  def transport(channel, message, dead_letter_channel) do
    try do
      GenServer.call(channel_name_spec(channel), message)
    catch
      :exit, _ ->
        transport(dead_letter_channel, message)
    end
  end
end
