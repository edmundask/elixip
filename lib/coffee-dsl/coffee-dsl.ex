defmodule CoffeeDSL do
  use Elixip.Integration, name: CoffeeShop.IntegrationDSL
  use Elixip.DSL

  # Integration is declared via DSL inline, although processor modules could be specified

  from "orders" do
    to "drinks", using: splitter fn message ->
      Elixip.delay()
      IO.puts("* Splitting...")

      Elixip.Message.get_body(message).items
    end
  end

  from "drinks" do
    router fn message ->
      IO.puts("* Routing...")
      Elixip.delay()

      case message do
        message_with_body %OrderItem{is_iced: true} -> "drinks:cold"
        message_with_body %OrderItem{is_iced: false} -> "drinks:hot"
      end
    end
  end

  from "drinks:cold" do
    to "deliveries", using: processor fn message ->
      {:ok, time} = Elixip.delay(3000)
      IO.puts("[*] Prepared COLD drink in: " <> Integer.to_string(time) <> "ms")

      message
    end
  end

  from "drinks:hot" do
    to "deliveries", using: processor fn message ->
      {:ok, time} = Elixip.delay(5000)
      IO.puts("[*] Prepared HOT drink in: " <> Integer.to_string(time) <> "ms")

      message
    end
  end

  from "deliveries" do
    to "serving", using: aggregator fn ->
      aggregation fn messages ->
        IO.puts("* Aggregating...")

        List.last(messages)
      end
    end
  end

  def endpoints do
    @endpoints
  end
end
