defmodule Coffee do
  use Elixip.Integration, name: CoffeeShop.Integration

  # All integration logic is scattered across different modules
  def endpoints do
    [
      {Orders.Endpoint, [name: "orders", to: "drinks"]},
      {Drinks.Endpoint, [name: "drinks", routes: %{cold: "drinks:cold", hot: "drinks:hot"}]},
      {ColdDrinks.Endpoint, [name: "drinks:cold", to: "deliveries"]},
      {HotDrinks.Endpoint, [name: "drinks:hot", to: "deliveries"]},
      {Deliveries.Endpoint, [name: "deliveries", to: "serving"]}
    ]
  end
end
