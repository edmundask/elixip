defmodule Orders.Endpoint do
  use Elixip.Endpoint

  @impl Elixip.Endpoint
  def processor do
    Orders.Splitter
  end
end
