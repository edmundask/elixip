defmodule Orders.Splitter do
  use Elixip.Processor
  use Elixip.Processor.Splitter

  def split(message) do
    Elixip.delay()
    IO.puts("* Splitting...")

    order = Elixip.Message.get_body(message)

    order.items
  end
end
