defmodule Deliveries.Aggregator do
  use Elixip.Processor
  use Elixip.Processor.Aggregator

  @impl Elixip.Processor.Aggregator
  def correlate(message) do
    Elixip.Message.get_body(message).order_id
  end

  @impl Elixip.Processor.Aggregator
  def aggregate(messages) do
    IO.puts("* Aggregating...")

    Elixip.Message.create(messages)
  end

  @impl Elixip.Processor.Aggregator
  def complete?(messages) do
    message = List.first(messages)
    expected_item_count = Elixip.Message.get_sequence_size(message)

    length(messages) == expected_item_count
  end
end
