defmodule Deliveries.Endpoint do
  use Elixip.Endpoint

  @impl Elixip.Endpoint
  def processor do
    Deliveries.Aggregator
  end
end
