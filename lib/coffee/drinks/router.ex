defmodule Drinks.Router do
  use Elixip.Processor
  use Elixip.Processor.Router
  use Elixip.DSL

  def route(message, routes) do
    IO.puts("* Routing...")
    Elixip.delay()

    case message do
      message_with_body(%OrderItem{is_iced: true}) ->
        IO.puts('* Received cold drink order')
        routes.cold
      message_with_body(%OrderItem{is_iced: false}) ->
        IO.puts('* Received hot drink order')
        routes.hot
    end
  end
end
