defmodule Drinks.Endpoint do
  use Elixip.Endpoint

  @impl Elixip.Endpoint
  def processor do
    Drinks.Router
  end
end
