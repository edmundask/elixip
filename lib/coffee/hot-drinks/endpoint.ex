defmodule HotDrinks.Endpoint do
  use Elixip.Endpoint

  @impl Elixip.Endpoint
  def processor do
    HotDrinks.Processor
  end
end
