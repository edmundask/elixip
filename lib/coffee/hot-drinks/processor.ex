defmodule HotDrinks.Processor do
  use Elixip.Processor
  use Elixip.Processor.Generic

  def process(message) do
    {:ok, time} = Elixip.delay(5000)
    IO.puts("[*] Prepared HOT drink in: " <> Integer.to_string(time) <> "ms")

    message
  end
end
