defmodule ColdDrinks.Processor do
  use Elixip.Processor
  use Elixip.Processor.Generic

  def process(message) do
    {:ok, time} = Elixip.delay(3000)
    IO.puts("[*] Prepared COLD drink in: " <> Integer.to_string(time) <> "ms")

    message
  end
end
