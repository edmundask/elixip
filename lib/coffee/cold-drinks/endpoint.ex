defmodule ColdDrinks.Endpoint do
  use Elixip.Endpoint

  @impl Elixip.Endpoint
  def processor do
    ColdDrinks.Processor
  end
end
