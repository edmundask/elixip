# Elixip

A proof-of-concept Elixir library that serves as an implementation of enterprise integration patterns (EIP).

## Examples

See inside `lib/` for comparison between module-based manual integration setup (`lib/coffee`) vs DSL-based auto-generated one (`lib/coffee-dsl`).
